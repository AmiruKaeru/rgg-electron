var output = '';

var search_results_product = document.getElementsByClassName('search_results_product');
var sr_rows = search_results_product[0].getElementsByClassName('sr_row');

for (let sr_row of sr_rows)
{
    var platform = sr_row.getElementsByClassName('sr_cell sr_platform')[0].innerText;
    var game = sr_row.getElementsByClassName('sr_cell sr_title')[0].innerText;
    var release = sr_row.getElementsByClassName('sr_cell sr_release')[0].innerText;

    output += platform + ' - ' + game + ' - ' + release + '\n';
}

console.log(output);